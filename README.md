# Boxinator
Boxinator is a web app created by a full-stack team at Experis Academy for Noroff Accelerated learning course. This app lets you order mystery boxes to be sent to multiple countries across Europe. These boxes have many different color & sizing options to choose from. You need to register an account in order to create shipments and view their status. Apart from normal registration, it is also possible to login with a Google account. Additionally, administrator accounts can be created as well. Admins can view all active shipments, change their status and edit country multipliers which affect pricing. 

Project was created with ASP.NET and React. You can use it here: https://boxinator-cd195.web.app/

Available backend endpoints can be found here: https://gitlab.com/experis-dotnet-case-study/boxinator-backend/-/wikis/Backend-Endpoints

## Features
- Account registration including Google sign-in and admin accounts. 
- Registered users can create shipments with following options:
    - Receiver
    - Weight tier
    - Box color
    - Destination country
- Registered users can also view their active and completed shipments as well as shipments' status history.
- Admins can view all active shipments and change their status.
- Admins can also view available destination countries and change their price multipliers. 
- All users can view and change their account details.

## Running the project locally
```
- To run project locally, you need to have Node.js and SQL Server installed on your computer.
- Clone or download front-end from https://gitlab.com/experis-dotnet-case-study/boxinator.git
- Clone or download back-end from https://gitlab.com/experis-dotnet-case-study/boxinator-backend.git
- From back-end directory open "Boxinator.sln" file in Visual Studio or another suitable IDE and run the following commands Package Manager Console:
    - Add-Migration SeedingData
    - Update-Database
- If needed, edit data source of default connection string in appsettings.json. It should be the same as your local SQL server. 
- Run the back-end from the IDE.
- In the root folder of front-end directory create a file called ".env". Add the following lines and enter back-end url inside the quotation marks if needed:
    {    
        REACT_APP_API_URL = "https://localhost:7272/api/"
    }
- Open front-end directory in terminal and run "npm i" to install dependencies. When finished run "npm start". Keep terminal window open.
```

## Contributors
- Maxim Anikin     https://gitlab.com/MaximusAnakin
- Valtteri Elo    https://gitlab.com/valtterielo
- Laura Koivuranta  https://gitlab.com/LauKoiFish
- Tuomo Kuusisto  https://gitlab.com/pampula

## License
[MIT](https://choosealicense.com/licenses/mit/)

﻿using Boxinator.Models.Enums;

namespace Boxinator.Tools
{
    public static class PriceCalculator
    {
        private const float FlatFee = 20.0f;

        public static float CalculatePrice(WeightTier tier, float countryMultiplier)
        {
            return FlatFee + GetWeightInKg(tier) * countryMultiplier;
        }

        private static int GetWeightInKg(WeightTier tier)
        {
            return tier switch
            {
                WeightTier.Basic => 1,
                WeightTier.Humble => 2,
                WeightTier.Deluxe => 5,
                WeightTier.Premium => 8,
            };
        }
    }
}
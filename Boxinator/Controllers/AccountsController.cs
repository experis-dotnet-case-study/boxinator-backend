using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Boxinator.Context;
using Boxinator.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using Boxinator.Models.DTOs.Account;
using Boxinator.Models.Enums;
using Microsoft.Net.Http.Headers;

namespace Boxinator.Controllers
{
    [Route("api/accounts")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class AccountsController : ControllerBase
    {
        private readonly PackageManagerDbContext _context;
        private readonly IMapper _mapper;
        
        public AccountsController(PackageManagerDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all accounts. [ADMIN]
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AccountReadDTO>>> GetAccounts()
        {
            if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
            return _mapper.Map<List<AccountReadDTO>>(
                await _context.Accounts
                    .Include(a => a.Shipments)
                    .ToListAsync());
        }
        
        /// <summary>
        /// Get a specific account. [NONE]
        /// </summary>
        /// <param name="id">FirebaseAccountId</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<AccountReadDTO>> GetAccount(string id)
        {
            // if (await AccountValidator.HasAccess(AccountType.Guest, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
            var account = await _context.Accounts.Include(a => a.Shipments)
                .FirstOrDefaultAsync(a => a.FirebaseAccountId == id);

            if (account == null)
            {
                return NotFound();
            }

            return _mapper.Map<AccountReadDTO>(account);
        }

        /// <summary>
        /// Update a specific account. [ADMIN]
        /// </summary>
        /// <param name="id">FirebaseAccountId</param>
        [HttpPut("id")]
        public async Task<IActionResult> PutAccount(string firebaseAccountId, AccountEditDTO dtoAccount)
        {
            if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
            if (firebaseAccountId != dtoAccount.FirebaseAccountId)
            {
                return BadRequest();
            }
            
            if (!AccountExists(firebaseAccountId))
            {
                return NotFound();
            }
            
            if (!Enum.IsDefined(typeof(AccountType), dtoAccount.AccountType))
            {
                return Problem($"AccountType {dtoAccount.AccountType} is undefined.");
            }

            // Account domainAccount = _mapper.Map<Account>(dtoAccount);
            Account domainAccount = await _context.Accounts.Where(a => a.FirebaseAccountId == dtoAccount.FirebaseAccountId).SingleOrDefaultAsync();
            domainAccount.Email = dtoAccount.Email;
            domainAccount.AccountType = dtoAccount.AccountType;
            Console.WriteLine($"New email: {domainAccount.Email}, new accountType: {domainAccount.AccountType}");
            // _context.Accounts.Attach(domainAccount).Property(x => x.Email).IsModified = true;
            // _context.Accounts.Attach(domainAccount).Property(x => x.AccountType).IsModified = true;
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return Ok();
        }

        /// <summary>
        /// Create a new account. [NONE]
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<AccountReadDTO>> PostAccount(AccountCreateDTO dtoAccount)
        {
            Account domainAccount = _mapper.Map<Account>(dtoAccount);
            _context.Accounts.Add(domainAccount);
            await _context.SaveChangesAsync();

            return _mapper.Map<AccountReadDTO>(domainAccount);
        }

        /// <summary>
        /// Delete a specific account. [ADMIN]
        /// </summary>
        /// <param name="id">AccountId</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccount(int id)
        {
            if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
            var account = await _context.Accounts.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }

            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool AccountExists(string firebaseId)
        {
            return _context.Accounts.Any(e => e.FirebaseAccountId == firebaseId);
        }
    }
}

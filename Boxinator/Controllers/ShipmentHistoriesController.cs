﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Boxinator.Context;
using Boxinator.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using Boxinator.Models.DTOs.ShipmentHistory;
using Boxinator.Models.Enums;
using Microsoft.Net.Http.Headers;

namespace Boxinator.Controllers
{
    [Route("api/shipmenthistories")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class ShipmentHistoriesController : ControllerBase
    {
        private readonly PackageManagerDbContext _context;
        private readonly IMapper _mapper;

        public ShipmentHistoriesController(PackageManagerDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get shipment histories for all shipments or a specific one. [ADMIN/GUEST]
        /// </summary>
        /// <param name="shipmentId">ShipmentId (optional)</param>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShipmentHistoryReadDTO>>> GetShipmentHistories(int? shipmentId)
        {
            if (shipmentId == null)
            {
                if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
                List<ShipmentHistoryReadDTO> shipmentHistories =_mapper.Map<List<ShipmentHistoryReadDTO>>(await _context.ShipmentHistories.ToListAsync());
                return FullShipmentInfo(shipmentHistories);
            }
            else
            {
                if (await AccountValidator.HasAccess(AccountType.Guest, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
                List<ShipmentHistoryReadDTO> shipmentHistories = _mapper.Map<List<ShipmentHistoryReadDTO>>(await _context.ShipmentHistories.Where(s => s.ShipmentId == shipmentId).ToListAsync());
                if (shipmentHistories.Count == 0)
                {
                    return NotFound();
                }
                else
                {
                    return FullShipmentInfo(shipmentHistories);
                }
            }
        }

        /// <summary>
        /// Delete specific shipmentHistory entry [ADMIN]
        /// </summary>
        /// <param name="id">ShipmentHistoryId</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShipmentHistory(int id)
        {
            if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
            var shipmentHistory = await _context.ShipmentHistories.FindAsync(id);
            if (shipmentHistory == null)
            {
                return NotFound();
            }

            _context.ShipmentHistories.Remove(shipmentHistory);
            await _context.SaveChangesAsync();

            return Ok();
        }
        
        List<ShipmentHistoryReadDTO> FullShipmentInfo(List<ShipmentHistoryReadDTO> shipmentHistories)
        {
            List<ShipmentHistoryReadDTO> modified = shipmentHistories;
            foreach (ShipmentHistoryReadDTO sDTO in modified)
            {
                sDTO.ShipmentStatusName = sDTO.ShipmentStatus.ToString();
            }

            return modified;
        }

        private bool ShipmentHistoryExists(int id)
        {
            return _context.ShipmentHistories.Any(e => e.Id == id);
        }
    }
}

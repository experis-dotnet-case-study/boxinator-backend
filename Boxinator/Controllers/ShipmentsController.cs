﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Boxinator.Context;
using Boxinator.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using Boxinator.Models.DTOs.Shipment;
using Boxinator.Models.Enums;
using Boxinator.Tools;
using Microsoft.Net.Http.Headers;

namespace Boxinator.Controllers
{
    [Route("api/shipments")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ShipmentsController : ControllerBase
    {
        private readonly PackageManagerDbContext _context;
        private readonly IMapper _mapper;

        public ShipmentsController(PackageManagerDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all shipments or for a specific account. [ADMIN/GUEST]
        /// </summary>
        /// <param name="firebaseAccountId">Filter by a specific accountId (optional)</param>
        /// <param name="shipmentStatus">Filter by specific shipment status (optional)</param>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetShipments(string? firebaseAccountId,
            ShipmentStatus? shipmentStatus)
        {
            // Admin
            if (firebaseAccountId == null)
            {
                if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
                if (shipmentStatus != null)
                {
                    List<ShipmentReadDTO> shipments = _mapper.Map<List<ShipmentReadDTO>>(await _context.Shipments
                        .Where(s => s.ShipmentStatus == shipmentStatus).Include(s => s.Country).ToListAsync());
                    return FullShipmentInfo(shipments);
                }
                else
                {
                    List<ShipmentReadDTO> shipments = _mapper.Map<List<ShipmentReadDTO>>(await _context.Shipments.Include(s => s.Country)
                        .ToListAsync());
                    return FullShipmentInfo(shipments);
                }
            }
            // Guest
            else
            {
                if (!AccountExists((firebaseAccountId)))
                {
                    return NotFound();
                }
                
                if (await AccountValidator.HasAccess(AccountType.Guest, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
                if (shipmentStatus != null)
                {
                    List<ShipmentReadDTO> shipments = _mapper.Map<List<ShipmentReadDTO>>(await _context.Shipments.Include(s => s.Country)
                        .Where(s => s.FirebaseAccountId == firebaseAccountId && s.ShipmentStatus == shipmentStatus)
                        .ToListAsync());
                    return FullShipmentInfo(shipments);
                }
                else
                {
                    List<ShipmentReadDTO> shipments = _mapper.Map<List<ShipmentReadDTO>>(await _context.Shipments.Include(s => s.Country)
                        .Where(s => s.FirebaseAccountId == firebaseAccountId).ToListAsync());
                    return FullShipmentInfo(shipments);
                }
            }
        }

        /// <summary>
        /// Get all completed shipments or for a specific account. [ADMIN/GUEST]
        /// </summary>
        /// <param name="firebaseAccountId">Filter by a specific FirebaseAccountId (optional)</param>
        [Route("completed")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCompleteShipments(string? firebaseAccountId)
        {
            return await GetShipments(firebaseAccountId, ShipmentStatus.Completed);
        }

        /// <summary>
        /// Get all cancelled shipments or for a specific account. [ADMIN/GUEST]
        /// </summary>
        /// <param name="firebaseAccountId">Filter by a specific FirebaseAccountId (optional)</param>
        [Route("cancelled")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCancelledShipments(string? firebaseAccountId)
        {
            return await GetShipments(firebaseAccountId, ShipmentStatus.Cancelled);
        }

        /// <summary>
        /// Get all active shipments or for a specific account. [ADMIN/GUEST]
        /// </summary>
        /// <param name="firebaseAccountId">Filter by a specific FirebaseAccountId (optional)</param>
        /// <returns></returns>
        [Route("active")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetActiveShipments(string? firebaseAccountId)
        {
            if (firebaseAccountId == null) // Admin
            {
                if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
                List<ShipmentReadDTO> shipments = _mapper.Map<List<ShipmentReadDTO>>(await _context.Shipments
                    .Where(s => (s.ShipmentStatus != ShipmentStatus.Completed &&
                                 s.ShipmentStatus != ShipmentStatus.Cancelled)).Include(s => s.Country).ToListAsync());
                return FullShipmentInfo(shipments);
            }
            else // User
            {
                if (await AccountValidator.HasAccess(AccountType.Guest, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
                List<ShipmentReadDTO> shipments = _mapper.Map<List<ShipmentReadDTO>>(await _context.Shipments
                    .Where(s => (s.ShipmentStatus != ShipmentStatus.Completed &&
                                 s.ShipmentStatus != ShipmentStatus.Cancelled)).Include(s => s.Country)
                    .Where(s => s.FirebaseAccountId == firebaseAccountId).ToListAsync());
                
                return FullShipmentInfo(shipments);
            }
        }

        List<ShipmentReadDTO> FullShipmentInfo(List<ShipmentReadDTO> shipments)
        {
            List<ShipmentReadDTO> modified = shipments;
            foreach (ShipmentReadDTO sDTO in modified)
            {
                sDTO.WeightTierName = sDTO.WeightTier.ToString();
                sDTO.ShipmentStatusName = sDTO.ShipmentStatus.ToString();
            }

            return modified;
        }

        /// <summary>
        /// Get a specific shipment. [REGISTERED]
        /// </summary>
        /// <param name="id">ShipmentId</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<ShipmentReadDTO>> GetShipment(int id)
        {
            if (await AccountValidator.HasAccess(AccountType.RegisteredUser, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();

            var shipment = await _context.Shipments.Include(s => s.Country).FirstOrDefaultAsync(s => s.Id == id);

            if (shipment == null)
            {
                return NotFound();
            }

            ShipmentReadDTO s = _mapper.Map<ShipmentReadDTO>(shipment);
            s.WeightTierName = shipment.WeightTier.ToString();
            s.ShipmentStatusName = shipment.ShipmentStatus.ToString();
            return s;
        }

        /// <summary>
        /// Get shipment price. [GUEST]
        /// </summary>
        [Route("price")]
        [HttpPost]
        public async Task<ActionResult<GetShipmentPriceResponseDTO>> GetShipmentPrice([FromBody] GetShipmentPriceRequestDTO dto)
        {
            if (await AccountValidator.HasAccess(AccountType.Guest, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();

            var country = await _context.Countries.FindAsync(dto.CountryId);

            if (country == null)
            {
                return BadRequest("Country not found.");
            }

            var price = PriceCalculator.CalculatePrice(dto.WeightTier, country.Multiplier);

            return Ok(new GetShipmentPriceResponseDTO { Price = price });
        }

        /// <summary>
        /// Update a specific shipment. [ADMIN]
        /// </summary>
        /// <param name="id">ShipmentId</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutShipment([FromRoute] int id, [FromBody] ShipmentEditDTO shipmentDTO)
        {
            if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();

            var targetShipment = await _context.Shipments.FindAsync(id);
            if (targetShipment == null)
            {
                return NotFound();
            }

            targetShipment.ShipmentStatus = shipmentDTO.ShipmentStatus;

            ShipmentHistory shipmentHistory = new ShipmentHistory
            {
                ShipmentId = targetShipment.Id,
                ShipmentStatus = targetShipment.ShipmentStatus,
                TimeStamp = DateTime.UtcNow
            };
            _context.ShipmentHistories.Add(shipmentHistory);

            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Create a new shipment. [GUEST]
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<ShipmentReadDTO>> PostShipment(ShipmentCreateDTO shipmentDto)
        {
            if (await AccountValidator.HasAccess(AccountType.Guest, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();

            Shipment newShipment = _mapper.Map<Shipment>(shipmentDto);
            newShipment.Date = DateTime.UtcNow;
            _context.Shipments.Add(newShipment);
            newShipment.Price = 0;

            try
            {
                var country = await _context.Countries.FindAsync(newShipment.CountryId);
                if (country == null)
                {
                    return BadRequest("Country not found.");
                }

                var price = PriceCalculator.CalculatePrice(newShipment.WeightTier, country.Multiplier);
                newShipment.Price = (float)price;
            }
            catch
            {
                return BadRequest("Error setting price.");
            }

            if (!Enum.IsDefined(typeof(WeightTier), shipmentDto.WeightTier))
            {
                return Problem($"Weight tier {shipmentDto.WeightTier} is undefined.");
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (!AccountExists(newShipment.FirebaseAccountId))
                {
                    return Problem($"AccountId {newShipment.FirebaseAccountId} not found.");
                }
                else if (!CountryExists(newShipment.CountryId))
                {
                    return Problem($"CountryId {newShipment.CountryId} not found.");
                }
                else
                {
                    return BadRequest();
                }
            }

            ShipmentHistory shipmentHistory = new ShipmentHistory();
            shipmentHistory.ShipmentId = newShipment.Id;
            shipmentHistory.ShipmentStatus = ShipmentStatus.Created;
            shipmentHistory.TimeStamp = DateTime.UtcNow;
            _context.ShipmentHistories.Add(shipmentHistory);

            await _context.SaveChangesAsync();

            ShipmentReadDTO s = _mapper.Map<ShipmentReadDTO>(newShipment);
            s.WeightTierName = s.WeightTier.ToString();
            s.ShipmentStatusName = s.ShipmentStatus.ToString();
            return s;
        }

        /// <summary>
        /// Delete a specific shipment. [ADMIN]
        /// </summary>
        /// <param name="id">ShipmentId</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShipment(int id)
        {
            if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();

            var shipment = await _context.Shipments.FindAsync(id);
            if (shipment == null)
            {
                return NotFound();
            }

            _context.Shipments.Remove(shipment);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool ShipmentExists(int id)
        {
            return _context.Shipments.Any(e => e.Id == id);
        }

        private bool AccountExists(string firebaseId)
        {
            return _context.Accounts.Any(e => e.FirebaseAccountId == firebaseId);
        }

        private bool CountryExists(int id)
        {
            return _context.Countries.Any(e => e.Id == id);
        }
    }
}
﻿using Boxinator.Context;
using Boxinator.Models.Domain;
using Boxinator.Models.Enums;
using FirebaseAdmin.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting.Internal;

namespace Boxinator.Controllers
{
    public static class AccountValidator
    {
        public static async Task<bool> HasAccess(AccountType requiredType, string? header, PackageManagerDbContext context)
        {
#if true
            return true;
#endif
             if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development") return true;
            if (header == null)
            {
                Console.WriteLine("Header is null.");
                return false;
            }
            Console.WriteLine("Header: " + header);
            var headerArray = header.Split(" ");
            var token = "";
            if (headerArray.Length < 2)
            {
                Console.WriteLine("Unauthorized.");
                return false;
            }
            else
            {
                token = headerArray[1];
            }
            Console.WriteLine("Token: " + header);

            try
            {
                string? uid = null;
                FirebaseToken decodedToken = await FirebaseAuth.DefaultInstance
                    .VerifyIdTokenAsync(idToken: token);
                    
                Console.WriteLine("Id token verified, uid: " + decodedToken.Uid);
                uid = decodedToken.Uid;

                Account? a = await context.Accounts.Where(e => e.FirebaseAccountId == uid).SingleOrDefaultAsync();
                if (a != null)
                {
                    return a.AccountType >= requiredType;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Verify token failed: " + e.Message);
                return false;
            }
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Boxinator.Context;
using Boxinator.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using Boxinator.Models.DTOs.Country;
using Boxinator.Models.Enums;
using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Net.Http.Headers;

namespace Boxinator.Controllers
{
    [Route("api/settings/countries")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class CountriesController : ControllerBase
    {
        private readonly PackageManagerDbContext _context;
        private readonly IMapper _mapper;

        public CountriesController(PackageManagerDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all countries. [GUEST]
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CountryReadDTO>>> GetCountries()
        {
            if (await AccountValidator.HasAccess(AccountType.Guest, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
            return _mapper.Map<List<CountryReadDTO>>(await _context.Countries.ToListAsync());
        }

        /// <summary>
        /// Get a specific country. [GUEST]
        /// </summary>
        /// <param name="id">Country ID</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<CountryReadDTO>> GetCountry(int id)
        {
            if (await AccountValidator.HasAccess(AccountType.Guest, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
            var country = await _context.Countries.FindAsync(id);

            if (country == null)
            {
                return NotFound();
            }

            return _mapper.Map<CountryReadDTO>(country);
        }

        /// <summary>
        /// Update a specific country. [ADMIN]
        /// </summary>
        /// <param name="id">Country ID.</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCountry(int id, CountryEditDTO countryDTO)
        {
            if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
            var targetCountry = await _context.Countries.FindAsync(id);
            if (targetCountry == null) {
                return NotFound();
            }

            targetCountry.Multiplier = countryDTO.Multiplier;
            await _context.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Create a new country. Name has to be unique. [ADMIN]
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Country>> PostCountry(CountryCreateDTO countryDto)
        {
            if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
            Country domainCountry = _mapper.Map<Country>(countryDto);
            _context.Countries.Add(domainCountry);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch(DbUpdateException)
            {
                if (CountryExists(domainCountry.Name))
                {
                    return BadRequest($"{domainCountry.Name} already exists on the list.");
                }
                else
                {
                    return BadRequest();
                }
            }

            return domainCountry;
        }

        /// <summary>
        /// Delete a specific country. [ADMIN]
        /// </summary>
        /// <param name="id">Country ID</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCountry(int id)
        {
            if (await AccountValidator.HasAccess(AccountType.Administrator, Request.Headers[HeaderNames.Authorization], _context) == false) return Unauthorized();
            var country = await _context.Countries.FindAsync(id);
            if (country == null)
            {
                return NotFound();
            }

            _context.Countries.Remove(country);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool CountryExists(int id)
        {
            return _context.Countries.Any(e => e.Id == id);
        }
        
        private bool CountryExists(string countryName)
        {
            return _context.Countries.Any(e => e.Name == countryName);
        }
    }
}

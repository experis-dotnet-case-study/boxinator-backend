﻿using Boxinator.Models.Enums;

namespace Boxinator.Models.DTOs.ShipmentHistory
{
    public class ShipmentHistoryReadDTO
    {
        // public int Id { get; set; }
        public int ShipmentId { get; set; }
        public DateTime TimeStamp { get; set; }
        public ShipmentStatus ShipmentStatus { get; set; }
        public string ShipmentStatusName { get; set; }
    }
}

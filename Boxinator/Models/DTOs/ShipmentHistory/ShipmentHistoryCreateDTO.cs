﻿using Boxinator.Models.Enums;

namespace Boxinator.Models.DTOs.ShipmentHistory
{
    public class ShipmentHistoryCreateDTO
    {
        public int ShipmentId { get; set; } 
        public DateTime TimeStamp { get; set; }
        public ShipmentStatus ShipmentStatus { get; set; }
    }
}

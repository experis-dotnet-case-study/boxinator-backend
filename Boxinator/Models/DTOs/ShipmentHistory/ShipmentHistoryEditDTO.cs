﻿using Boxinator.Models.Enums;

namespace Boxinator.Models.DTOs.ShipmentHistory
{
    public class ShipmentHistoryEditDTO
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public ShipmentStatus ShipmentStatus { get; set; }
    }
}

﻿using Boxinator.Models.Enums;

namespace Boxinator.Models.DTOs.Shipment
{
    public class ShipmentReadDTO
    {
        public int Id { get; set; }
        public string FirebaseAccountId { get; set; }
        public DateTime Date { get; set; }
        public string Receiver { get; set; }
        public WeightTier WeightTier { get; set; }
        public string WeightTierName { get; set; }
        public float Price { get; set; }
        public string Colour { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public ShipmentStatus ShipmentStatus { get; set; }
        public string ShipmentStatusName { get; set; }
    }
}

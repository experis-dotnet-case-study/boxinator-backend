﻿namespace Boxinator.Models.DTOs.Shipment
{
    public class GetShipmentPriceResponseDTO
    {
        public float Price { get; set; }
    }
}

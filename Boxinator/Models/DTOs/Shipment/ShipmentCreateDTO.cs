﻿using System.ComponentModel.DataAnnotations;
using Boxinator.Models.Enums;

namespace Boxinator.Models.DTOs.Shipment
{
    public class ShipmentCreateDTO
    {
        [Required]
        public string Receiver { get; set; }
        public WeightTier WeightTier { get; set; }
        public string Colour { get; set; }
        [Required]
        public string FirebaseAccountId { get; set; }
        [Required]
        public int CountryId { get; set; }
    }
}

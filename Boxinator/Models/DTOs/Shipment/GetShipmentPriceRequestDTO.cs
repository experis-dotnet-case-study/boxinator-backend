﻿using Boxinator.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace Boxinator.Models.DTOs.Shipment
{
    public class GetShipmentPriceRequestDTO
    {
        [Required]
        public int CountryId { get; set; }
        [Required]
        public WeightTier WeightTier { get; set; }
    }
}

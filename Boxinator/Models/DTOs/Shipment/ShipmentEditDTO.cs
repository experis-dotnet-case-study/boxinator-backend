﻿using Boxinator.Models.Enums;

namespace Boxinator.Models.DTOs.Shipment
{
    public class ShipmentEditDTO
    {
        public ShipmentStatus ShipmentStatus { get; set; }
    }
}

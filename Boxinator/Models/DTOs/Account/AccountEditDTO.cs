﻿using Boxinator.Models.Enums;

namespace Boxinator.Models.DTOs.Account
{
    public class AccountEditDTO
    {
        public string FirebaseAccountId { get; set; }
        public string Email { get; set; }
        public AccountType AccountType { get; set; }
    }
}

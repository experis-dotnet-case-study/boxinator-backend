﻿using Boxinator.Models.Domain;
using Boxinator.Models.Enums;

namespace Boxinator.Models.DTOs.Account
{
    public class AccountReadDTO
    {
        public int Id { get; set; }
        public string FirebaseAccountId { get; set; }
        public string Email { get; set; }
        public AccountType AccountType { get; set; }
    }
}

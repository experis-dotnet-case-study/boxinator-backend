﻿namespace Boxinator.Models.DTOs.Account
{
    public class AccountCreateDTO
    {
        public string FirebaseAccountId { get; set; }
        public string Email { get; set; }
        public int AccountType { get; set; }
    }
}

﻿namespace Boxinator.Models.DTOs.Country
{
    public class CountryEditDTO
    {
        public float Multiplier { get; set; }
    }
}

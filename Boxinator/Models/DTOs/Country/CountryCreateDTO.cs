﻿namespace Boxinator.Models.DTOs.Country
{
    public class CountryCreateDTO
    {
        public string Name { get; set; }
        public float Multiplier { get; set; }
    }
}

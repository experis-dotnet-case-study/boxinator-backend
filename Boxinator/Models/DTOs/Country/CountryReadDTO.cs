﻿namespace Boxinator.Models.DTOs.Country
{
    public class CountryReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float Multiplier { get; set; }
    }
}

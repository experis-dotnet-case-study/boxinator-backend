﻿namespace Boxinator.Models.Enums
{
    public enum AccountType
    {
        Guest, RegisteredUser, Administrator
    }
    
    public enum ShipmentStatus
    {
        Created, Received, InTransit, Completed, Cancelled
    }

    public enum WeightTier
    {
        Basic, Humble, Deluxe, Premium
    }
}
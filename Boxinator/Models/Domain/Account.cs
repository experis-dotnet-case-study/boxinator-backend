﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Boxinator.Models.Enums;

namespace Boxinator.Models.Domain
{
    [Table("Account")]
    public class Account
    {
        // Primary key
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(100)]
        public string FirebaseAccountId { get; set; }
        [Required]
        public AccountType AccountType { get; set; }
        [MaxLength(60)]
        public string Email { get; set; }
        // Relationships
        public ICollection<Shipment>? Shipments { get; set; }
    }
}

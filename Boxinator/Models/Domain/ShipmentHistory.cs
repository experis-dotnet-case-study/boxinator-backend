﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Boxinator.Models.Enums;

namespace Boxinator.Models.Domain
{
    [Table("ShipmentHistory")]
    public class ShipmentHistory
    {
        // Primary key
        public int Id { get; set; }
        // Fields
        [Required]
        public DateTime TimeStamp { get; set; }
        [Required]
        public ShipmentStatus ShipmentStatus { get; set; }

        // Relationships
        [Required]
        public int ShipmentId { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Boxinator.Models.Domain
{
    [Table("Country")]
    public class Country
    {
        // Primary key
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string? Name { get; set; }
        [Required]
        public float Multiplier { get; set; }
    }
}

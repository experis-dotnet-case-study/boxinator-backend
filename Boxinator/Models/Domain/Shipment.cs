﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Boxinator.Models.Enums;

namespace Boxinator.Models.Domain
{
    [Table("Shipment")]
    public class Shipment
    {
        // Primary key
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string FirebaseAccountId { get; set; }
        // Fields
        [Required]
        public DateTime Date { get; set; }
        [Required]
        [MaxLength(100)]
        public string Receiver { get; set; }
        [Required]
        public WeightTier WeightTier { get; set; }
        [Required]
        public float Price { get; set; }
        [Required]
        [MaxLength(6)]
        public string Colour { get; set; }
        [Required]
        public ShipmentStatus ShipmentStatus { get; set; }

        // Relationships
        [Required]
        public int CountryId { get; set; }
        public Country Country { get; set; }

    }
}

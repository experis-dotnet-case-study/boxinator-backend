﻿using AutoMapper;
using Boxinator.Models.Domain;
using Boxinator.Models.DTOs.Account;

namespace Boxinator.Models.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<Account, AccountReadDTO>().ReverseMap();
            CreateMap<AccountCreateDTO, Account>();
            CreateMap<Account, AccountCreateDTO>();
            CreateMap<AccountEditDTO, Account>();

        }
    }
}

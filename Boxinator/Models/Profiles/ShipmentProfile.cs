﻿using AutoMapper;
using Boxinator.Models.Domain;
using Boxinator.Models.DTOs.Shipment;

namespace Boxinator.Models.Profiles
{
    public class ShipmentProfile : Profile
    {
        public ShipmentProfile()
        {
            // Shipment -> ShipmentReadDTO
            CreateMap<Shipment, ShipmentReadDTO>()
                .ForMember(sdto => sdto.CountryName, opt => opt
                    .MapFrom(s => s.Country.Name))
                .ReverseMap();
            // Shipment -> ShipmentCreateDTO
            CreateMap<ShipmentCreateDTO, Shipment>();
            // Shipment -> ShipmentEditDTO
            CreateMap<ShipmentEditDTO, Shipment>();
        }
    }
}
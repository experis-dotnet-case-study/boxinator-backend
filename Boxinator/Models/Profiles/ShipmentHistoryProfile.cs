﻿using AutoMapper;
using Boxinator.Models.Domain;
using Boxinator.Models.DTOs.ShipmentHistory;

namespace Boxinator.Models.Profiles
{
    public class ShipmentHistoryProfile : Profile
    {
        public ShipmentHistoryProfile()
        {
                // Shipment -> ShipmentReadDTO
            CreateMap<ShipmentHistory, ShipmentHistoryReadDTO>()
                .ReverseMap();
            // Shipment -> ShipmentCreateDTO
            CreateMap<ShipmentHistoryCreateDTO, ShipmentHistory>();
            // Shipment -> ShipmentEditDTO
            CreateMap<ShipmentHistoryEditDTO, ShipmentHistory>();
        }
    }
}
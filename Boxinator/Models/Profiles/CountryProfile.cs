﻿using AutoMapper;
using Boxinator.Models.Domain;
using Boxinator.Models.DTOs.Country;

namespace Boxinator.Models.Profiles
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            CreateMap<Country, CountryReadDTO>()
                .ReverseMap();
            CreateMap<CountryCreateDTO, Country>()
                .ReverseMap();
            CreateMap<CountryEditDTO, Country>()
                .ReverseMap();
        }
    }
}

﻿using Boxinator.Models.Domain;
using Boxinator.Models.Enums;
using Microsoft.EntityFrameworkCore;

namespace Boxinator.Context
{
    public class PackageManagerDbContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<ShipmentHistory> ShipmentHistories { get; set; }

        public PackageManagerDbContext(DbContextOptions<PackageManagerDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .HasIndex(a => new { a.FirebaseAccountId })
                .IsUnique(true);

            modelBuilder.Entity<Account>().HasData(
                new Account { Id = 1, AccountType = AccountType.RegisteredUser, Email = "test@test.com", FirebaseAccountId = "TestFirebaseUID" },
                new Account { Id = 2, AccountType = AccountType.RegisteredUser, Email = "qwe@qwe.com", FirebaseAccountId = "TestFirebaseUID2" },
                new Account { Id = 3, AccountType = AccountType.Administrator, Email = "adming@admin.com", FirebaseAccountId = "TestFirebaseUID3" },
                new Account { Id = 4, AccountType = AccountType.Guest, Email = "guest@guest.com", FirebaseAccountId = "TestFirebaseUID4" }
            );
            
            modelBuilder.Entity<Country>()
                .HasIndex(c => new { c.Name })
                .IsUnique(true);
            
            modelBuilder.Entity<Country>().HasData(
                new Country { Id = 1, Name = "Norway", Multiplier = 0 },
                new Country { Id = 2, Name = "Sweden", Multiplier = 0 },
                new Country { Id = 3, Name = "Denmark", Multiplier = 0 },
                new Country { Id = 4, Name = "Finland", Multiplier = 1 },
                new Country { Id = 5, Name = "Netherlands", Multiplier = 1 },
                new Country { Id = 6, Name = "Germany", Multiplier = 1 },
                new Country { Id = 7, Name = "France", Multiplier = 1.5F },
                new Country { Id = 8, Name = "Switzerland", Multiplier = 1.5F },
                new Country { Id = 9, Name = "Spain", Multiplier = 2 },
                new Country { Id = 10, Name = "Italy", Multiplier = 2 }
                );
            
            modelBuilder.Entity<Shipment>().HasData(
                new Shipment { Id = 1, FirebaseAccountId = "TestFirebaseUID", Receiver = "TestReceiver1", Colour = "f25d20", Price = 100, WeightTier = WeightTier.Basic, CountryId = 1, ShipmentStatus = ShipmentStatus.Created, Date = DateTime.UtcNow},
                new Shipment { Id = 2, FirebaseAccountId = "TestFirebaseUID2", Receiver = "TestReceiver2", Colour = "f25d20", Price = 50, WeightTier = WeightTier.Humble, CountryId = 2, ShipmentStatus = ShipmentStatus.InTransit, Date = new DateTime(2022, 10, 10)},
                new Shipment { Id = 3, FirebaseAccountId = "TestFirebaseUID2", Receiver = "TestReceiver3", Colour = "f25d20", Price = 10, WeightTier = WeightTier.Premium, CountryId = 3, ShipmentStatus = ShipmentStatus.Cancelled, Date = new DateTime(2022, 9, 2)}
            );

            modelBuilder.Entity<ShipmentHistory>().HasData(
                new ShipmentHistory{ Id = 1, ShipmentId = 1, ShipmentStatus = ShipmentStatus.Created, TimeStamp = DateTime.UtcNow },
                new ShipmentHistory{ Id = 2, ShipmentId = 2, ShipmentStatus = ShipmentStatus.Created, TimeStamp = new DateTime(2022, 10, 10) },
                new ShipmentHistory{ Id = 3, ShipmentId = 3, ShipmentStatus = ShipmentStatus.Created, TimeStamp = new DateTime(2022, 9, 2) },
                new ShipmentHistory{ Id = 4, ShipmentId = 3, ShipmentStatus = ShipmentStatus.InTransit, TimeStamp = new DateTime(2022, 9, 15) },
                new ShipmentHistory{ Id = 5, ShipmentId = 2, ShipmentStatus = ShipmentStatus.Received, TimeStamp = new DateTime(2022, 10, 15) },
                new ShipmentHistory{ Id = 6, ShipmentId = 3, ShipmentStatus = ShipmentStatus.Cancelled, TimeStamp = DateTime.UtcNow}
            );
        }
    }
}

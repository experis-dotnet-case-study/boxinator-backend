using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;

namespace Boxinator;

public class Program
{
    public static void Main(string[] args)
    {
        if (FirebaseApp.DefaultInstance == null)
        {
            FirebaseApp.Create(new AppOptions()
            {
                Credential = GoogleCredential.FromFile(".\\boxinator-service-account-file.json")
            });
        }
        
        CreateHostBuilder(args).Build().Run();
    }
    
    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
}
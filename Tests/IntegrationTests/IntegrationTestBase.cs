﻿using Boxinator;
using Boxinator.Models.DTOs.Account;
using Boxinator.Models.DTOs.Shipment;
using Boxinator.Models.Enums;
using Microsoft.AspNetCore.Mvc.Testing;

namespace Tests.IntegrationTests
{
    [TestFixture]
    public class IntegrationTestBase
    {
        public const string BASE_URL = "http://localhost:5272/";
        protected const string TestAccountFirebaseId = "Test12354782";
        protected const string TestAccountEmail = "test@test123892.com";
        protected int TestAccountId = -1;
        protected int TestShipmentId = -1;

        protected const string TestReceiver = "TestReceiver";
        protected const WeightTier TestWeightTier = WeightTier.Humble;
        protected const string TestColour = "F00000";
        protected const int TestCountryId = 1;

        protected CustomWebApplicationFactory<Program> _application;

        [OneTimeSetUp]
        public void SetUp()
        {
            _application = new CustomWebApplicationFactory<Program>();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
        }

        protected HttpClient GetHttpClient()
        {
            var clientOptions = new WebApplicationFactoryClientOptions
            {
                BaseAddress = new Uri(BASE_URL),
                AllowAutoRedirect = true,
                HandleCookies = true,
                MaxAutomaticRedirections = 10
            };

            return _application.CreateClient(clientOptions);
        }

        protected async Task RegisterTestAccountAsync()
        {
            var client = GetHttpClient();

            var payload = new AccountCreateDTO
            {
                FirebaseAccountId = TestAccountFirebaseId,
                Email = TestAccountEmail
            };

            // Act
            var response = await client.PostAsJsonAsync("api/Accounts", payload);

            if (!response.IsSuccessStatusCode)
                throw new Exception("Could not register test user!");

            var dto = response.Content.ReadFromJsonAsync<AccountReadDTO>();
            TestAccountId = dto.Id;
        }
        
        protected async Task CreateTestShipmentAsync()
        {
            var client = GetHttpClient();
            var payload = new ShipmentCreateDTO()
            {
                Receiver = TestReceiver,
                Colour = TestColour,
                CountryId = TestCountryId,
                FirebaseAccountId = TestAccountFirebaseId,
                WeightTier = TestWeightTier
            };

            // Act
            var response = await client.PostAsJsonAsync("api/shipments", payload);

            if (!response.IsSuccessStatusCode)
                throw new Exception("Could not create a shipment!");

            var dto = response.Content.ReadFromJsonAsync<ShipmentReadDTO>();
            TestShipmentId = dto.Id;
        }
    }
}

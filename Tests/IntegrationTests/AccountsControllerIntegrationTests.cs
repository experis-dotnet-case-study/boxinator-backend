﻿using Boxinator.Context;
using Boxinator.Models.DTOs.Account;
using Boxinator.Models.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Tests.IntegrationTests
{
    internal class AccountsControllerIntegrationTests : IntegrationTestBase
    {
        [SetUp]
        public async Task SetUp()
        {
            await RegisterTestAccountAsync();
        }

        [Test]
        public async Task CanRegisterAccountSuccessfully()
        {
            // Arrange
            const string expectedFirebaseId = "Foo";
            const string expectedEmail = "foo@foo.com";

            var client = GetHttpClient();

            var payload = new AccountCreateDTO
            {
                FirebaseAccountId = expectedFirebaseId,
                Email = expectedEmail,
                AccountType = 1
            };

            // Act
            var response = await client.PostAsJsonAsync("api/Accounts", payload);
            var dto = response.Content.ReadFromJsonAsync<AccountReadDTO>();

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);

            using var scope = _application.Services.CreateScope();
            var provider = scope.ServiceProvider;
            using var dbContext = provider.GetRequiredService<PackageManagerDbContext>();

            var createdAccount = await dbContext.Accounts.SingleAsync(x => x.FirebaseAccountId == expectedFirebaseId);

            Assert.Multiple(() =>
            {
                Assert.That(createdAccount.Email, Is.EqualTo(expectedEmail));
                Assert.That(createdAccount.FirebaseAccountId, Is.EqualTo(expectedFirebaseId));
                Assert.That(createdAccount.AccountType, Is.EqualTo(AccountType.RegisteredUser));
            });
        }

        [Test]
        public async Task UserCanGetOwnAccount()
        {

        }
    }
}

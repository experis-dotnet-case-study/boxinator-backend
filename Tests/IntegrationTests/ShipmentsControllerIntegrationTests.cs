﻿using Boxinator.Context;
using Boxinator.Models.DTOs.Shipment;
using Boxinator.Tools;
using Boxinator.Models.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Tests.IntegrationTests
{
    internal class ShipmentsControllerIntegrationTests : IntegrationTestBase
    {
        [SetUp]
        public async Task SetUp()
        {
            await CreateTestShipmentAsync();
        }
        
        [Test]
        public async Task CanCreateShipmentSuccessfully()
        {
            // Arrange
            const string expectedFirebaseId = "Foo";
            const string expectedColour = "f00000";
            const string expectedReceiver = "TestReceiver1";
            const WeightTier expectedWeightTier = WeightTier.Deluxe;
            const int expectedCountryId = 5;
            const float expectedCountryMultiplier = 1;
            float expectedPrice = PriceCalculator.CalculatePrice(expectedWeightTier, expectedCountryMultiplier);
            
            var client = GetHttpClient();

            var payload = new ShipmentCreateDTO()
            {
                Receiver = expectedReceiver,
                WeightTier = expectedWeightTier,
                Colour = expectedColour,
                FirebaseAccountId = expectedFirebaseId,
                CountryId = expectedCountryId
            };

            // Act
            var response = await client.PostAsJsonAsync("api/Shipments", payload);
            var dto = response.Content.ReadFromJsonAsync<ShipmentReadDTO>();
            var responseText = await response.Content.ReadAsStringAsync();
            dynamic valuePoco = Newtonsoft.Json.JsonConvert.DeserializeObject(responseText);
            int actualId = int.Parse(Convert.ToString(valuePoco.id));
            
            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            using var scope = _application.Services.CreateScope();
            var provider = scope.ServiceProvider;
            using var dbContext = provider.GetRequiredService<PackageManagerDbContext>();
            var createdShipment = await dbContext.Shipments.SingleAsync(x => x.Id == actualId);
            Assert.AreEqual(createdShipment.Id, actualId);
            Assert.Multiple(() =>
            {
                Assert.That(createdShipment.FirebaseAccountId, Is.EqualTo(expectedFirebaseId));
                Assert.That(createdShipment.Receiver, Is.EqualTo(expectedReceiver));
                Assert.That(createdShipment.Colour, Is.EqualTo(expectedColour));
                Assert.That(createdShipment.CountryId, Is.EqualTo(expectedCountryId));
                Assert.That(createdShipment.WeightTier, Is.EqualTo(expectedWeightTier));
                Assert.That(createdShipment.Price, Is.EqualTo(expectedPrice));
            });
        }
    }
}